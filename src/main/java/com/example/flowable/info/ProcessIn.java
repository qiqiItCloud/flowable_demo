package com.example.flowable.info;

import org.flowable.engine.runtime.ProcessInstance;

import java.util.Date;

/**
 * @author Foolish
 * @description
 * @date: 2018/9/26 10:24
 */
public class ProcessIn {
    String id;
    String deploymentId;
    String description;
    String name;
    String tenantId;
    String businessKey;
    String callbackId;
    String callbackType;
    String localizedDescription;
    String localizedName;
    String processDefinitionId;
    String processDefinitionKey;
    String processDefinitionName;
    String processDefinitionVersion;
    Date startTime;
    String startUserId;
    String processInstanceId;
    String activityId;
    String parentId;
    String rootProcessInstanceId;
    String superExecutionId;

    public ProcessIn(ProcessInstance pi) {
        this.id = pi.getId();
        this.deploymentId = pi.getDeploymentId();
        this.description = pi.getDescription();
        this.name = pi.getName();
        this.tenantId = pi.getTenantId();
        this.businessKey = pi.getBusinessKey();
        this.callbackId = pi.getCallbackId();
        this.callbackType = pi.getCallbackType();
        this.localizedDescription = pi.getLocalizedDescription();
        this.localizedName = pi.getLocalizedName();
        this.processDefinitionId = pi.getProcessDefinitionId();
        this.processDefinitionKey = pi.getProcessDefinitionKey();
        this.processDefinitionName = pi.getProcessDefinitionName();
        this.processDefinitionVersion = pi.getProcessDefinitionId();
        this.startTime = pi.getStartTime();
        this.startUserId = pi.getStartUserId();
        this.processInstanceId = pi.getProcessInstanceId();
        this.activityId = pi.getActivityId();
        this.parentId = pi.getParentId();
        this.rootProcessInstanceId = pi.getRootProcessInstanceId();
        this.superExecutionId = pi.getSuperExecutionId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getCallbackId() {
        return callbackId;
    }

    public void setCallbackId(String callbackId) {
        this.callbackId = callbackId;
    }

    public String getCallbackType() {
        return callbackType;
    }

    public void setCallbackType(String callbackType) {
        this.callbackType = callbackType;
    }

    public String getLocalizedDescription() {
        return localizedDescription;
    }

    public void setLocalizedDescription(String localizedDescription) {
        this.localizedDescription = localizedDescription;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public String getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(String processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getRootProcessInstanceId() {
        return rootProcessInstanceId;
    }

    public void setRootProcessInstanceId(String rootProcessInstanceId) {
        this.rootProcessInstanceId = rootProcessInstanceId;
    }

    public String getSuperExecutionId() {
        return superExecutionId;
    }

    public void setSuperExecutionId(String superExecutionId) {
        this.superExecutionId = superExecutionId;
    }

    @Override
    public String toString() {
        return "ProcessIn{" +
                "id='" + id + '\'' +
                ", deploymentId='" + deploymentId + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", tenantId='" + tenantId + '\'' +
                ", businessKey='" + businessKey + '\'' +
                ", callbackId='" + callbackId + '\'' +
                ", callbackType='" + callbackType + '\'' +
                ", localizedDescription='" + localizedDescription + '\'' +
                ", localizedName='" + localizedName + '\'' +
                ", processDefinitionId='" + processDefinitionId + '\'' +
                ", processDefinitionKey='" + processDefinitionKey + '\'' +
                ", processDefinitionName='" + processDefinitionName + '\'' +
                ", processDefinitionVersion='" + processDefinitionVersion + '\'' +
                ", startTime=" + startTime +
                ", startUserId='" + startUserId + '\'' +
                ", processInstanceId='" + processInstanceId + '\'' +
                ", activityId='" + activityId + '\'' +
                ", parentId='" + parentId + '\'' +
                ", rootProcessInstanceId='" + rootProcessInstanceId + '\'' +
                ", superExecutionId='" + superExecutionId + '\'' +
                '}';
    }
}
