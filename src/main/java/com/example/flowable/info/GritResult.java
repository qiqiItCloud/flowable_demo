package com.example.flowable.info;

/**
 * @author Foolish
 * @description
 * @date: 2018/9/25 16:25
 */
public class GritResult {
    String status;
    int totals;
    Object data;

    public GritResult(String status, int totals, Object data) {
        this.status = status;
        this.totals = totals;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotals() {
        return totals;
    }

    public void setTotals(int totals) {
        this.totals = totals;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
